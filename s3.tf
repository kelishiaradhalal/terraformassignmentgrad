resource "aws_s3_bucket" "terraform-assignment-bucket" {
  bucket = var.aws_bucket_name
  acl    = "private"
  versioning {
    enabled = false
  }
  tags = {
    Name = var.aws_bucket_name
  }
}

resource "aws_s3_bucket_public_access_block" "s3Public" {
  bucket                  = aws_s3_bucket.terraform-assignment-bucket.id
  block_public_acls       = true
  ignore_public_acls      = true
  block_public_policy     = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_notification" "s3_notification" {
  bucket = aws_s3_bucket.terraform-assignment-bucket.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.s3_triggered_rek_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_suffix       = ".png"
  }
}
