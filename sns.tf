locals {
  email_addresses = ["kelishia@synthesis.co.za"]
  # add more email recipients if required
}

resource "aws_sns_topic" "topic" {
  name = "s3_upload_notification"
}

resource "aws_sns_topic_subscription" "topic_email_subscription" {
  count     = length(local.email_addresses)
  topic_arn = aws_sns_topic.topic.arn
  protocol  = "email"
  endpoint  = local.email_addresses[count.index]
}
