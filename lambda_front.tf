locals {
  lambda_front_zip_location = "${path.module}/outputs/front_fxn.zip"
}

data "archive_file" "front_fxn" {
  type        = "zip"
  source_file = "${path.module}/front_fxn.js"
  output_path = local.lambda_front_zip_location
}

resource "aws_lambda_function" "api_triggered_lambda" {
  filename      = local.lambda_front_zip_location
  depends_on    = [data.archive_file.front_fxn]
  function_name = "front_fxn_lambda"
  role          = aws_iam_role.lambda_front_role.arn
  timeout       = 45
  handler       = "front_fxn.handler"

  source_code_hash = "$filebase64sha256(local.lambda_front_zip_location)"

  runtime = "nodejs12.x"
}

resource "aws_lambda_permission" "allow_api" {
  statement_id  = "AllowAPIgatewayInvocation"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.api_triggered_lambda.arn
  principal     = "apigateway.amazonaws.com"
}
