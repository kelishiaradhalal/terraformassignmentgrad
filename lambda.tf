locals {
  lambda_zip_location = "${path.module}/outputs/back_fxn.zip"
}

data "archive_file" "back_fxn" {
  type        = "zip"
  source_file = "${path.module}/back_fxn.js"
  output_path = local.lambda_zip_location
}

resource "aws_lambda_function" "s3_triggered_rek_lambda" {
  filename      = local.lambda_zip_location
  depends_on    = [data.archive_file.back_fxn]
  function_name = "back_fxn_lambda"
  role          = aws_iam_role.lambda_role.arn
  timeout       = 45
  handler       = "back_fxn.handler"

  source_code_hash = "$filebase64sha256(local.lambda_zip_location)"

  runtime = "nodejs12.x"
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.s3_triggered_rek_lambda.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.terraform-assignment-bucket.arn
}
