var AWS = require('aws-sdk');
AWS.config.update({ region: 'eu-west-1' });

const docClient = new AWS.DynamoDB.DocumentClient();

let listItems = async(params) => {
  try {
    const data = await docClient.scan(params).promise()
    return data
  } catch (err) {
    return err
  }
}

exports.handler = async (event, context) => {
  const ddbparams = {
    TableName: 'ddb-baseball-cards'
  }
  
  try {
    const data = await listItems(ddbparams)
    var response = {
    statusCode: 200,
    headers: {
      'Content-Type': 'text/html; charset=utf-8',
    }
    };
    
    return { body: JSON.stringify(data) }
  } catch (err) {
    return { error: err }
  }

  
  callback(null, response);
};