resource "aws_api_gateway_rest_api" "api-lambda" {
  name        = "API-Lambda"
  description = "API Gateway to Lambda"
}


resource "aws_api_gateway_resource" "resource" {
  rest_api_id = aws_api_gateway_rest_api.api-lambda.id
  parent_id   = aws_api_gateway_rest_api.api-lambda.root_resource_id
  #   path_part   = "{proxy+}"
  path_part = "api-resource"
}

resource "aws_api_gateway_method" "api-method" {
  rest_api_id   = aws_api_gateway_rest_api.api-lambda.id
  resource_id   = aws_api_gateway_resource.resource.id
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "lambda-int" {
  rest_api_id = aws_api_gateway_rest_api.api-lambda.id
  resource_id = aws_api_gateway_resource.resource.id
  http_method = aws_api_gateway_method.api-method.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.api_triggered_lambda.invoke_arn
}

resource "aws_api_gateway_method" "proxy_root" {
  rest_api_id   = aws_api_gateway_rest_api.api-lambda.id
  resource_id   = aws_api_gateway_rest_api.api-lambda.root_resource_id
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "lambda_root" {
  rest_api_id = aws_api_gateway_rest_api.api-lambda.id
  resource_id = aws_api_gateway_method.proxy_root.resource_id
  http_method = aws_api_gateway_method.proxy_root.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.api_triggered_lambda.invoke_arn
}

resource "aws_api_gateway_deployment" "api-deploy" {
  depends_on = [
    "aws_api_gateway_integration.lambda-int",
    "aws_api_gateway_integration.lambda_root",
  ]

  rest_api_id = aws_api_gateway_rest_api.api-lambda.id
  stage_name  = "Prod"
}

resource "aws_lambda_permission" "apigw" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.api_triggered_lambda.function_name
  principal     = "apigateway.amazonaws.com"

  # The /*/* portion grants access from any method on any resource
  # within the API Gateway "REST API".
  source_arn = "${aws_api_gateway_rest_api.api-lambda.execution_arn}/Prod/POST/api-resource"
}

output "base_url" {
    value = aws_api_gateway_deployment.api-deploy.invoke_url
}
