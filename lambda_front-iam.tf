resource "aws_iam_role_policy" "lambda_front_policy" {
    name = "lambda_front_policy_kelishia"
    role = "${aws_iam_role.lambda_front_role.id}"
    policy = "${file("iam/lambda-front-policy.json")}"
}


resource "aws_iam_role" "lambda_front_role"{
    name = "lambda_front_role_attachment"
    assume_role_policy = "${file("iam/lambda-assumePolicy.json")}"
}
