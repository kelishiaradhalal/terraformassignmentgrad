var AWS = require('aws-sdk');
AWS.config.update({ region: 'eu-west-1' });

const ddb = new AWS.DynamoDB.DocumentClient();

// 'use strict';

exports.handler = function (event, context, callback) {
  var ddbparams = {
    TableName: 'ddb-baseball-cards'
  }
  
  var items = getDDBItems(ddbparams);

  var response = {
    statusCode: 200,
    headers: {
      'Content-Type': 'text/html; charset=utf-8',
    },
    body: items[0]
    };
  callback(null, response);
};

let getDDBItems = (params) => {
  const scanResults = [];
    const items = [];
    do{
        items =  ddb.scan(params).promise().then((res) => {
          items.Items.forEach((item) => scanResults.push(item));
          params.ExclusiveStartKey  = items.LastEvaluatedKey;
        });
    }while(typeof items.LastEvaluatedKey !== "undefined");
    
    return scanResults;
}