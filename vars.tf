variable "aws_region" {
  type        = string
  description = "AWS region"
}

variable "aws_bucket_name" {
  type        = string
  description = "Name of the S3 bucket"
}

variable "aws_dynamo_DB_name"{
  type = string
  description = "Name of DynamoDB table"
}